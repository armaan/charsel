# charsel
A simple bash script to make copying frequently used special characters to your clipboard easy.

It uses a plain-text file known as a "charfile" to define easy to reach shortcuts to special characters

## Installation
- Run `make` as root to install
- Run `make uninstall` as root to remove
- Run `make purge` to remove charsel files from your home directory
- Run `make install-completions` to install the shell completions

## Dependencies
  * Bash
  * Linux
  * Xclip (using the -n flag bypasses this)
  * A terminal with UTF-8 encoding enabled (most default terminals will be fine)

## Usage
For argument usage, use `charsel -h`

Once you have started the program, simply type the shortcut listed on the left side of the table to copy the character on the right side of the table to your clipboard. Shortcodes the same length as the longest one will automatically be entered. To quit, type ';' as a shortcode

Some of the default charfiles have hidden shortcuts for capital letters that are not shown on the table. Simply type a capital version of the shortcut key to access the capital version of the special character.

## Creating a charfile
Make a new file in `~/.local/share/charsel/charfiles/` or `/usr/share/charsel/charfiles/` with the name that you would like to use to call the charfile in the command.

Fill in your shortcodes, using the provided examples in `/usr/share/charsel/charfiles` as a template. There are just 4 rules to follow.
  1. The pattern used is "shortcode,output"
  2. Hidden shortcodes go above the line with "---"
  3. Lines can be commented out using a "#"
  4. Shortcodes may not contain a semicolon or a hyphen

## License
Copyright Armaan Bhojwani 2020, MIT License, see the LICENSE file for more information
